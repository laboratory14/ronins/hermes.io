<?php

function echoln(string $line): void {
    echo $line . PHP_EOL;
}

function echolnind(string $line, int $indent_times = 4): void {
    echo str_repeat('>', $indent_times) . ' ' . $line . PHP_EOL;
}

function redis_connect(): ?Redis {
    $config = require 'redis.php';

    $redis = new Redis();

    if (! $redis->connect($config['host'], $config['port'])) {
        throw new RuntimeException('Can`t connect to Redis');
    }

    return $redis;
}
