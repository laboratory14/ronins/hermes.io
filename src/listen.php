<?php

require_once '../vendor/autoload.php';
require_once 'lib.php';

$redis = redis_connect();

$group = 'crypto';
$consumer = 'websocket';

$crypto_keys = $redis->keys($group . ':*');
$streams = [];

foreach ($crypto_keys as $stream_name) {
    $streams[$stream_name] = '>';
    // init consumer group
    $redis->xGroup('create', $stream_name, $group, '>');
}

//exit(0);

/*
 * init consumer group
 */

while (true) {
    $entries = $redis->xReadGroup($group, $consumer, $streams, $count = 1, $block = true);

    foreach ($entries as $entry) {
//        echolnind(print_r($entry));
        $message_id = array_keys($entry)[0];
        $message = $entry[$message_id];
        $redis->xAck($message['stream'], $group, [$message_id]);
        echolnind('Message processed: '. $message_id);
    }
}