<?php

require_once '../vendor/autoload.php';
require_once 'lib.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

const CCOMP_KEY = '58e9dea904a6d281760ca8a0aad237f88333ebc1794fc962f02087f6d8a48896';

function build_ccomp_url(string $key, string $fsyms, string $tsyms): string {
    return sprintf(
        "https://min-api.cryptocompare.com/data/pricemulti?fsyms=%s&tsyms=%s&api_key=%s",
        $fsyms, $tsyms, $key
    );
}

/**
 * @throws GuzzleException
 */
function get_ticket_data(string $url): string
{
    $guzzle = new Client();
    $response = $guzzle->get($url);
    $status_code = $response->getStatusCode();
    if ($status_code !== 200) {
        throw new \DomainException('Api returned status code: ' . $status_code);
    }
    return $response->getBody();
}

/**
 * @throws JsonException
 */
function convert_to_json(string $data): array {
    return json_decode($data, true, 512, JSON_THROW_ON_ERROR);
}

function generate_redis_stream_key(string $prefix, string $fsyms, string $tsyms): string {
    return sprintf("%s:%s:%s", $prefix, $fsyms, $tsyms);
}

function redis_stream_data(float $price_original, int $price_cents): array {
    return compact('price_original', 'price_cents');
}

function get_ticket_price_original(array $data, string $fsym, string $tsym): float {
    return $data[$fsym][$tsym];
}

function get_ticket_price_cents(array $data, string $fsym, string $tsym): int {
    return ($data[$fsym][$tsym] * 100);
}

function redis_put_stream(Redis $redis, string $stream, array $data): bool {
    return $redis->xadd($stream, '*', $data);
}

function redis_get_stream_last(Redis $redis, string $stream): array
{
    return $redis->xRevRange($stream, '+', '-', 1);
}

/*
 * test data
 */

$ticket_quotes = [
    'crypto' => [
        ['BTC', 'USD'],
        ['BTC', 'EUR'],
        ['BTC', 'UAH'],
        ['ETH', 'USD'],
        ['ETH', 'EUR'],
        ['ETH', 'UAH'],
    ]
];

/*
 * get data from provider
 */

$pipeline_data = [];

foreach (array_keys($ticket_quotes) as $prefix) {
    $tickets = $ticket_quotes[$prefix];
    echoln($prefix);

    foreach ($tickets as [$from, $to]) {
        $ticket_urL = build_ccomp_url(CCOMP_KEY, $from, $to);
        echolnind('Ticket URL: ' . $ticket_urL);

        $stream_key = generate_redis_stream_key($prefix, $from, $to);
        echolnind('Redis KEY: ' . $stream_key);

        $ticket_data = [];
        try {
            $ticket_data = convert_to_json(get_ticket_data($ticket_urL));
        } catch (GuzzleException | JsonException $e) {
            echolnind('Error: ' . $e->getMessage());
        }
        echolnind('Ticket DATA: ' . print_r($ticket_data, true));

        $stream_data = redis_stream_data(
            get_ticket_price_original($ticket_data, $from, $to),
            get_ticket_price_cents($ticket_data, $from, $to),
        );
        echolnind('Put to PIPELINE: ' . print_r($stream_data));
        $pipeline_data[$stream_key] = $stream_data;
    }
}


/*
 * set streams in pipeline
 */

echoln('Put data to STREAMS...');

$redis = redis_connect();

$pipeline = $redis->pipeline();

foreach ($pipeline_data as $stream => $data) {
    $data['stream'] = $stream;
    $pipeline->xadd($stream, '*', $data);
}

$pipeline->exec();

/*
 * get streams
 */

//echoln('Get data from streams...');
//
//foreach (array_keys($ticket_quotes) as $prefix) {
//    $tickets = $ticket_quotes[$prefix];
//    echoln($prefix);
//
//    foreach ($tickets as [$from, $to]) {
//        $stream_key = generate_redis_stream_key($prefix, $from, $to);
//        echolnind('Redis KEY: ' . $stream_key);
//        $stream_data = redis_get_stream_last($redis, $stream_key);
//        echolnind('Get from Redis Stream: ' . print_r($stream_data, true));
//    }
//}
//
echoln('Redis keys: ' . print_r($redis->keys('*')));